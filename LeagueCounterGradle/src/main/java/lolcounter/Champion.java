package lolcounter;


public class Champion {

	private long championNumber;
	private String championName;
	private String counter;

	/**
	 * Represents a customer with customer number, first and last name.
	 * @param firstName
	 * @param lastName
	 */
	public Champion(String Name, String Counter) {
		this.championName = Name;
		this.counter = Counter;
	}

	/**
	 * Returns the customer number
	 * @return the customer number
	 */
	public long getchampionNumber() {
		return championNumber;
	}


	/**
	 * Returns the champions name
	 * @return the Champions name
	 */
	public String getchampionName() {
		return championName;
	}

	/**
	 * Returns the Counter to the champion
	 * @return the Counter to the champion
	 */
	public String getCounter() {
		return counter;
	}
	
	/**
	 * Sets the customer number
	 * @param number
	 */
	public void setchampionNumber(long number) {
		this.championNumber = number;
	}

	/**
	 * Sets the champion name
	 * @param champion name
	 */
	public void setChampionName(String Name) {
		this.championName = Name;
	}

	/**
	 * Sets the last name
	 * @param lastName
	 */
	public void setCounterChampion(String Counter) {
		this.counter= Counter;
	}

	@Override
	public String toString() {
		return "Champion [number=" + championNumber + ", name=" + championName + " " + counter + "]";
	}

	/** 
	 * Customers are equal *only* if their customer numbers are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Champion other = (Champion) obj;
		if (championNumber != other.championNumber)
			return false;
		return true;
	}	

}