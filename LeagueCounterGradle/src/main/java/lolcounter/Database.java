package lolcounter;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


public class Database {
	
    private static Map<Long, Champion> championDb = new HashMap<Long, Champion>();
	private static final AtomicLong championNumber = new AtomicLong();
    
    /**
     * Get the Champion database
     * @return the customer database
     */
    public static Map<Long, Champion> getChampionDb() {
    	return championDb;
    }

    /**
     * Get the Customer counter
     * @return the customer counter
     */
    public static AtomicLong getChampionNumber() {
    	return championNumber;
    }
}