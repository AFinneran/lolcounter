package lolcounter;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
public class ChampionCounter {

	private static final AtomicLong number = Database.getChampionNumber();
    private static Map<Long, Champion> championDb = Database.getChampionDb();
  
    /**
     * Create a new champion in the database
     * @param champion with name and counter
     * @return the champion number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/champion/new")
    public ResponseEntity<Long> addNewChampion(@RequestBody Champion champion) {
    	champion.setchampionNumber(number.incrementAndGet());
    	championDb.put(champion.getchampionNumber(), champion);
    	return new ResponseEntity<>(champion.getchampionNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a champion from the database
     * @param number the champion number
     * @return the champion if in the database, not found if not
     */
    @GetMapping("/champions/{number}")
    public ResponseEntity<Object> getChampionByNumber(@PathVariable long number) {
    	if (championDb.containsKey(number)) {
            return new ResponseEntity<>(championDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Champion does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    /**
     * Create a new champion in the database
     * @param champion adc with name and counter
     * @return the champion number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/champion/adc/new")
    public ResponseEntity<Long> addNewADC(@RequestBody Champion champion) {
    	champion.setchampionNumber(number.incrementAndGet());
    	championDb.put(champion.getchampionNumber(), champion);
    	return new ResponseEntity<>(champion.getchampionNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a champion from the database
     * @param number the champion number
     * @return the champion if in the database, not found if not
     */
    @GetMapping("/champions/adc/{number}")
    public ResponseEntity<Object> getADCByNumber(@PathVariable long number) {
    	if (championDb.containsKey(number)) {
            return new ResponseEntity<>(championDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Champion does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    
    /**
     * Create a new champion in the database
     * @param champion adc with name and counter
     * @return the champion number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/champion/support/new")
    public ResponseEntity<Long> addNewSupport(@RequestBody Champion champion) {
    	champion.setchampionNumber(number.incrementAndGet());
    	championDb.put(champion.getchampionNumber(), champion);
    	return new ResponseEntity<>(champion.getchampionNumber(), HttpStatus.CREATED);
    }
 
    /**
     * Get a champion from the database
     * @param number the champion number
     * @return the champion if in the database, not found if not
     */
    @GetMapping("/champions/support/{number}")
    public ResponseEntity<Object> getSupportByNumber(@PathVariable long number) {
    	if (championDb.containsKey(number)) {
            return new ResponseEntity<>(championDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Champion does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    
    @GetMapping("/champions/all")
    public ResponseEntity<Object> getChampionObjectArray (){
    	
    	ArrayList<Champion> myChampion = new ArrayList(number.intValue()); 
    	if(number.intValue()> 0) {
    	for (long i = 1; i <= number.intValue(); i++) {
    		myChampion.add(championDb.get(i));
    	}
    	return new ResponseEntity<Object>(myChampion, HttpStatus.OK);
    }
    	
    	else { 
    		return new ResponseEntity<>("Length of Array is 0, add Champions to it please.", HttpStatus.NOT_FOUND);
    	}
    }    
    
    @PutMapping("/champions/{number}")
    public ResponseEntity<Object> getChampionChangedNumber(@PathVariable long number, @RequestBody Champion champion) {
    	if (championDb.containsKey(number)){
    		championDb.get(number).setChampionName(champion.getchampionName());
            return new ResponseEntity<>(championDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Champion does not exist", HttpStatus.NOT_FOUND);
    	}
    }
    /*    
    /**
     * Unimplemented feature
     *
     * Get a champion from the database
     * @param number the customer number
     * @return the champion if in the database, not found if not
     *
    @GetMapping("/champions/{string}")
    public ResponseEntity<Object> getChampionByNumber(@PathVariable String string) {
    	ArrayList<Champion> myChampion = new ArrayList(number.intValue()); 
    	if(number.intValue()> 0) {
    	for (long i = 1; i <= number.intValue(); i++) {
    		myChampion.add(championDb.get(i));
    	}
    	//return new ResponseEntity<Object>(myChampion, HttpStatus.OK);
    	for (long j = 1; j <= number.intValue(); j++) {
    		Champion temp = new Champion(null, null);
    		temp = myChampion.get((int) j);
    		if(temp.getchampionName()== string) {
    			return new ResponseEntity<Object>(temp, HttpStatus.OK);
    		}
    		else {
    			j++;
    		}
    	}
    }   	
    	else { 
    		return new ResponseEntity<>("Length of Array is 0, add Champions to it please.", HttpStatus.NOT_FOUND);
    	}
    	return null;
    }
    */
}