import { Component } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Champion} from './champion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'League Of Legends Counter';
  champion: Champion = null;
  support: Champion = null;
  adc: Champion = null;
  error: HttpErrorResponse = null;

  constructor(private http: HttpClient) {
  }

  getADCByNumber(number: number): void {
    this.http.get<Champion>('http://localhost:8080/champions/adc/' + number)
      .subscribe(data => {
          this.adc = data;
        },
        (error: HttpErrorResponse) => {
          console.log(error.error);
          this.adc = new Champion();
          this.error = error;
        }
      );

  }
  getSupportByNumber(number: number): void {
    this.http.get<Champion>('http://localhost:8080/champions/support/' + number)
      .subscribe(data => {
          this.support = data;
        },
        (error: HttpErrorResponse) => {
          console.log(error.error);
          this.support = new Champion();
          this.error = error;
        }
      );

  }

  getChampionByNumber(number: number): void {
    this.http.get<Champion>('http://localhost:8080/champions/' + number)
      .subscribe(data => {
          this.champion = data;
        },
        (error: HttpErrorResponse) => {
          console.log(error.error);
          this.champion = new Champion();
          this.error = error;
        }
      );

  }
}
